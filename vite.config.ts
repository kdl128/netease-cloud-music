import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'

// https://vitejs.dev/config/
export default ({ mode })=>{ 
  // 获取.env文件里定义的环境变量
  // 可加第三个参数 loadEnv(mode, process.cwd(),"")设置第三个参数为 '' 来加载所有环境变量，而不管是否有 `VITE_` 前缀。
  const env=loadEnv(mode, process.cwd());

  return defineConfig({
    plugins: [vue()],
    resolve: {
      alias: {
        '@': resolve(__dirname, 'src'),
      }
    },
    base: './', // 打包路径
    server: {
      port: 4090, // 服务端口号
      open: true, // 服务启动时是否自动打开浏览器
      cors: true, // 允许跨域
      proxy: {
        '/api': {
          target: env.VITE_BASE_URL,
          changeOrigin: true,
          rewrite: (path) => path.replace(/^\/api/, '')
        }
      }
    }
  })
}
