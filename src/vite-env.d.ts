
// 声明文件扩充

/// <reference types="vite/client" />

// 自定义环境变量的类型
interface ImportMetaEnv {
  readonly VITE_BASE_URL: string;
  // 更多环境变量...
}
declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  export default component
}