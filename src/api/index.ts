import request from '@/utils/request'
// 获取云音乐首页新碟上架数据
export const getAlbumNewest = () => {
  return request({
    url: '/album/newest',
    method: 'get',
  })
}