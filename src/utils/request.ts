// 封装axios
import axios from 'axios'

// 使用自定义配置新建一个 axios 实例
const service = axios.create({
  baseURL: `/api`, // `baseURL` 将自动加在 `url` 前面，除非 `url` 是一个绝对 URL (便于为 axios 实例的方法传递相对 URL);这里设置为/api，在vite.config.ts中进行替换
  timeout: 15000, // 设置超时时间
  withCredentials: true // 表示跨域请求时需要使用凭证
})
// 请求拦截器
service.interceptors.request.use(
  (config) => {
    // 请求前操作
    return config
  },
  (error) => {
    // 请求错误
    return Promise.reject(error)
  }
)

// 响应拦截器
service.interceptors.response.use(
  (response) => {
    let res = response.data
    return res
  },
  (error) => {
    console.log('请求响应错误：',error);
    return Promise.reject(error)
  }
)

export default service